package com.ranginkaman.myapplication

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.ranginkaman.myapplication.viewmodel.MainViewModel

class MainActivity : AppCompatActivity() {

    lateinit var viewModel: MainViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        viewModel.user.observe(this, Observer {
            println("DEBUG: $it")
        })

        viewModel.setPostId("5")

//        println("DEBUG: ${ExampleSingletons.singletonUser.hashCode()}")
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.cancelJob()
    }
}
