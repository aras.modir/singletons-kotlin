package com.ranginkaman.myapplication.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.ranginkaman.myapplication.models.User
import com.ranginkaman.myapplication.repository.Repository

class MainViewModel : ViewModel() {

    private val _userId: MutableLiveData<String> = MutableLiveData()

    val user: LiveData<User> = Transformations
        .switchMap(_userId) {
            Repository.getUser(it)
        }

    fun setPostId(postId: String) {
        val update = postId
        if (_userId.value == update) {
            return
        }
        _userId.value = update
    }

    fun cancelJob() {
        Repository.cancelJob()
    }
}