package com.ranginkaman.myapplication.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class User(

    @Expose
    @SerializedName("userId")
    val userID: Long? = null,

    @Expose
    @SerializedName("id")
    val id: Long? = null,

    @Expose
    @SerializedName("title")
    val title: String? = null,

    @Expose
    @SerializedName("body")
    val body: String? = null
) {
    override fun toString(): String {
        return "User(userId: $userID, id: $id, title: $title, body: $body)"
    }
}
