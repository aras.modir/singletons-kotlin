package com.ranginkaman.myapplication.singletons

import com.ranginkaman.myapplication.models.User

object ExampleSingletons {

    val singletonUser: User by lazy {
        User(1, 1, "sunt aut facere", "quia et suscipit nsuscipit recusandae")
    }
}