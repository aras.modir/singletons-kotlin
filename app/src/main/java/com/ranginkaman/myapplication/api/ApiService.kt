package com.ranginkaman.myapplication.api

import com.ranginkaman.myapplication.models.User
import retrofit2.http.*

interface ApiService {
    @GET("posts/{postId}/")
    suspend fun getUsers(
        @Path("postId") postId: String
    ): User
}